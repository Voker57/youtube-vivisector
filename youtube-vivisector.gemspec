Gem::Specification.new do |s|
  s.name        = 'youtube-vivisector'
  s.version     = '0.0.2'
  s.date        = '2019-05-10'
  s.summary     = "youtube stream ripper"
  s.description = ""
  s.authors     = ["voker57"]
  s.email       = 'voker57@gmail.com'
  s.executables = ["youtube-vivisector"]
  s.homepage    =
    'https://gitlab.com/Voker57/youtube-vivisector'
  s.license       = 'MIT'
  s.add_runtime_dependency 'nokogiri', '~> 1.10'
end
